Init SSL without certificate database
battery.charge: 77
battery.voltage: 12.40
battery.voltage.high: 13.00
battery.voltage.low: 10.40
battery.voltage.nominal: 12.0
device.type: ups
driver.name: blazer_usb
driver.parameter.pollinterval: 2
driver.parameter.port: auto
driver.parameter.synchronous: auto
driver.version: 2.8.0
driver.version.internal: 0.14
driver.version.usb: libusb-1.0.26 (API: 0x1000109)
input.current.nominal: 3.0
input.frequency: 0.0
input.frequency.nominal: 50
input.voltage: 6.1
input.voltage.fault: 6.1
input.voltage.nominal: 230
output.voltage: 231.7
ups.beeper.status: enabled
ups.delay.shutdown: 30
ups.delay.start: 180
ups.load: 1
ups.productid: 0000
ups.status: OB
ups.type: offline / line interactive
ups.vendorid: 0001
